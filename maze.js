document.addEventListener('keydown', movePlayer);
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
let playerRow = -1, playerCol = -1;
createBoard();

function createBoard(){
    /*for(let i = 0; i < map.length; i ++){
        let temp = document.createElement("div");
        temp.class = "row";
        temp.id = "row" + i;
        for(let j = 0;  j < map[i].length; j ++){
            let cell = document.createElement("div");
            cell.class = "cell";
            cell.id = "cellr" + i + "c" + j;
            cell.innerHTML = map[i][j];
            if(map[i][j] == " ")
                cell.innerHTML = '&nbsp;';
            if(map[i][j] == "S"){
                playerCol = j;
                playerRow = i;
                map[i][j] = "O";
            }
            temp.appendChild(cell);
        }
        document.getElementById("gameBoard").appendChild(temp);
    }*/
    for(let j = 0; j < map[0].length; j ++){
        let temp = document.createElement("div");
        temp.class = "row";
        temp.id = "row" + j;
        for(let i = 0 ; i < map.length; i ++){
            let cell = document.createElement("div");
            cell.class = "cell";
            cell.id = "cellr" + i + "c" + j;
            cell.innerHTML = map[i][j];
            if(map[i][j] == " ")
                cell.innerHTML = '&nbsp;';
            if(map[i][j] == "S"){
                playerCol = j;
                playerRow = i;
                map[i][j] = "O";
            }
            temp.appendChild(cell);
        }
        document.getElementById("gameBoard").appendChild(temp);
    }
}

function movePlayer(e){
    if(e.key == "ArrowDown"){
        if(map[playerRow + 1][playerCol] != "W"){
            document.getElementById("cellr" + (playerRow + 1) + "c" + playerCol).innerHTML = "X";
            document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = "&nbsp";
            playerRow ++;
        }
    }
    else if(e.key == "ArrowLeft"){
        if(map[playerRow][playerCol - 1] != "W"){
            document.getElementById("cellr" + (playerRow) + "c" + (playerCol - 1)).innerHTML = "X";
            document.getElementById("cellr" + playerRow + "c" + playerCol).innerHTML = "&nbsp";
            playerCol --;
        }
    }
    else if(e.key == "ArrowRight"){
        if(map[playerRow][playerCol + 1] != "W"){
            document.getElementById("cellr" + (playerRow) + "c" + (playerCol + 1)).innerHTML = "X";
            document.getElementById("cellr" + playerRow + "c" + playerCol).innerHTML = "&nbsp";
            playerCol ++;
        }
    }
    else if(e.key == "ArrowUp"){
        if(map[playerRow - 1][playerCol] != "W"){
            document.getElementById("cellr" + (playerRow - 1) + "c" + playerCol).innerHTML = "X";
            document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = "&nbsp";
            playerRow --;
        }
    }
    if(map[playerRow][playerCol] == "F"){
        document.getElementById("gameBoard").innerHTML = "";
        let temp = document.createElement("h1");
        temp.innerHTML = "!!!YOU WIN!!!"
        document.getElementById("gameBoard").appendChild(temp);
    }
}